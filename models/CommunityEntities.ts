'use strict'

module.exports.city = (sequelize, type) => {
    return sequelize.define('city', {
        city_id: {
        type: type.INTEGER,
        allowNull: false,
        autoIncrement: true
        },
        city_name: {
        type: type.STRING,
        primaryKey: true,
        allowNull: false,
        unique: true
        },
        is_preferred: {
        type: type.BOOLEAN,
        allowNull: false,
        defaultValue: false
        }
    },{
        freezeTableName: true,
        timestamps: false
    })
}