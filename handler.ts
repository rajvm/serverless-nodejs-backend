"use strict";
let AWS = require("aws-sdk");
let lambda = new AWS.Lambda();

const axios = require("axios").default;
const apiKey = "77b7c39fdcd57288b58a09503dd4afd0";
const weatherApiUrl =
  "http://api.openweathermap.org/data/2.5/weather?units=metric";
//local
// const mysql = require("serverless-mysql")({
//   config: {
//     host: "localhost",
//     port: 3306,
//     database: "mysql1",
//     user: "root",
//     password: "Welcome123@",
//   },
// });

const mysql = require("serverless-mysql")({
  config: {
    host: "tm1fpwvi0m2qxjl.cdgkfoacvf6u.us-east-1.rds.amazonaws.com",
    port: 3306,
    database: "mysql1",
    user: "root",
    password: "Welcome123",
  },
});

/**
 * Base response HTTP headers
 */
const responseHeaders = {
  "Content-Type": "application/json",
  "Access-Control-Allow-Origin": "*", // Required for CORS support to work
  "Access-Control-Allow-Credentials": true, // Required for cookies, authorization headers with HTTPS
};

/**
 * HTTP response templates
 */
const responses = {
  success: (data = {}, code = 200) => {
    return {
      statusCode: code,
      headers: responseHeaders,
      body: JSON.stringify(data),
    };
  },
  error: (error) => {
    return {
      statusCode: error.code || 500,
      headers: responseHeaders,
      body: JSON.stringify(error),
    };
  },
};

exports.getWeather = async (event, context) => {
  let results;
  let city = event.pathParameters.city_name;
  console.log(city);
  try {
    // Get weather for the zip code provided
    const response = await axios({
      url: `${weatherApiUrl}&q=${city}&appid=${apiKey}`,
      method: "get",
      port: 443,
      responseType: JSON,
    });
    results = {
      description: response.data.weather[0].description,
      temp: response.data.main.temp,
      feels_like: response.data.main.feels_like,
      name: response.data.name,
      error: null,
    };

    let queryResult = await mysql.query(
      "SELECT city_name as name FROM city where city_name = ?",
      [response.data.name]
    );
    console.log('queryResult@'+queryResult+'@');

    if (queryResult.length < 1) {
      try {
        console.log('inside !queryResult'+queryResult);
        let queryResults = await mysql
          .transaction()
          .query("INSERT INTO city (city_name,is_preferred) VALUES(?,?)", [
            response.data.name,
            0,
          ])
          .rollback((e) => {
            /* do something with the error */
            console.log(e);
            results = "connection issue";
          }) // optional
          .commit();
      } catch (err) {
        if (err.code === "ER_DUP_ENTRY") {
          console.log(err);
          results = "City already exists";
        } else {
          console.log(err);
          results = "Error in creating city in getWeather()";
        }
      }
    }
  } catch (err) {
    console.error("getWeather error: ", err);
    results = {
      error: "Sorry, there was a problem with the weather service.",
    };
  }
  return responses.success(results);
};

exports.getAutoCompleteCityName = async (event, context) => {
  const city_name = event.queryStringParameters.city;

  let results = await mysql.query(
    "SELECT city_name as name FROM city where city_name like ?",
    ["%" + city_name + "%"]
  );

  // Return the results
  return responses.success(results);
};

exports.getPreferredCities = async (event, context) => {
  // Run query
  let results = await mysql.query(
    "SELECT city_name FROM city where is_preferred is true"
  );

  // Return the results
  console.log(JSON.stringify(results));

  return results;
};

exports.getAllCities = async (event, context) => {
  // Run query
  let results = await mysql.query("SELECT * FROM city");

  // Return the results
  return results;
};

exports.updatePreferredForCity = async (event, context) => {
  let results;

  // let cityName = event.pathParameters.city_name;
  // const data = JSON.parse(event.body);
  const cityName = event.queryStringParameters.city;
  const subscribeAlert = event.queryStringParameters.isPreferred;
  console.log(subscribeAlert);
  console.log(cityName);

  // Run query
  results = await mysql
    .transaction()
    .query("Update city set is_preferred = ? where city_name =?", [
      subscribeAlert,
      cityName,
    ])
    .rollback((e) => {
      /* do something with the error */
      console.log(e);
      results = "connection issue";
    }) // optional
    .commit();

  // Return the results
  return responses.success(results);
};

exports.createCity = async (event, context) => {
  let results;

  try {
    const data = JSON.parse(event.body);
    console.log(data.city);
    console.log(data.isPreferred);

    // Run query
    results = await mysql
      .transaction()
      .query("INSERT INTO city (city_name,is_preferred) VALUES(?,?)", [
        data.city,
        data.isPreferred,
      ])
      .rollback((e) => {
        /* do something with the error */
        console.log(e);
        results = "connection issue";
      }) // optional
      .commit();
  } catch (err) {
    if (err.code === "ER_DUP_ENTRY") {
      console.log(err);
      results = "City already exists";
    } else {
      console.log(err);
      results = "Error in createCity";
    }
  }

  // Return the results
  return results;
};

exports.sendWeatherAlert = async (event) => {
  const funcName = "sendWeatherAlert";
  console.log("entering" + funcName);

  let params = {
    FunctionName: "serverless-weather-app-dev-getPreferredCities",
    InvocationType: "RequestResponse",
    Payload: '{ "foo" : "bar" }',
  };
  let resp;

  await lambda
    .invoke(params, function (err, data) {
      if (err) {
        throw err;
      } else {
        console.log(
          "FIRST serverless-weather-app-dev-getPreferredCities invoked: " +
            data.Payload
        );
        //resp = data.Payload;
        resp = JSON.stringify(data.Payload);
      }
    })
    .promise();

  let params1 = {
    FunctionName: "serverless-weather-app-dev-getWeatherForEmail",
    InvocationType: "RequestResponse",
    Payload: resp,
  };

  await lambda
    .invoke(params1, function (err, data) {
      if (err) {
        throw err;
      } else {
        let response = JSON.parse(data.Payload);
        console.log(
          "SECOND serverless-weather-app-dev-getWeatherForEmail invoked: " +
            response.body
        );
        resp = JSON.stringify(response.body);
      }
    })
    .promise();

  let params2 = {
    FunctionName: "serverless-weather-app-dev-sendEmail",
    InvocationType: "RequestResponse",
    Payload: resp,
  };

  await lambda
    .invoke(params2, function (err, data) {
      if (err) {
        throw err;
      } else {
        let response = JSON.parse(data.Payload);
        console.log(
          "SECOND serverless-weather-app-dev-getWeatherForEmail invoked: " +
            response.body
        );
        resp = JSON.stringify(response.body);
      }
    })
    .promise();
};

exports.getWeatherForEmail = async (event, context) => {
  let weatherJsonObj;
  let weatherJsonArr = [];
  console.log(JSON.parse(event));
  let cityArr = JSON.parse(event);
  //cityArr.forEach(async function (value) {
  for (let i = 0; i < cityArr.length; i++) {
    //console.log(value.city_name);

    //let city = value.city_name
    let city = cityArr[i].city_name;
    console.log(city);
    try {
      // Get weather for the zip code provided
      const response = await axios({
        url: `${weatherApiUrl}&q=${city}&appid=${apiKey}`,
        method: "get",
        port: 443,
        responseType: JSON,
      });
      weatherJsonObj = {
        description: response.data.weather[0].description,
        temp: response.data.main.temp,
        feels_like: response.data.main.feels_like,
        name: response.data.name,
        error: null,
      };
    } catch (err) {
      console.error("getWeather error: ", err);
      weatherJsonObj = {
        error: "Sorry, there was a problem with the weather service.",
      };
    }
    console.log("got weather sir" + JSON.stringify(weatherJsonObj));
    weatherJsonArr.push(weatherJsonObj);
    // });
  }
  console.log("exiting getWeatherForEmail" + JSON.stringify(weatherJsonArr));
  return {
    statusCode: 200,
    body: JSON.stringify(weatherJsonArr),
  };
};
