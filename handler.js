"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var AWS = require("aws-sdk");
var lambda = new AWS.Lambda();
var axios = require("axios")["default"];
var apiKey = "77b7c39fdcd57288b58a09503dd4afd0";
var weatherApiUrl = "http://api.openweathermap.org/data/2.5/weather?units=metric";
//local
// const mysql = require("serverless-mysql")({
//   config: {
//     host: "localhost",
//     port: 3306,
//     database: "mysql1",
//     user: "root",
//     password: "Welcome123@",
//   },
// });
var mysql = require("serverless-mysql")({
    config: {
        host: "tm1fpwvi0m2qxjl.cdgkfoacvf6u.us-east-1.rds.amazonaws.com",
        port: 3306,
        database: "mysql1",
        user: "root",
        password: "Welcome123"
    }
});
/**
 * Base response HTTP headers
 */
var responseHeaders = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": true
};
/**
 * HTTP response templates
 */
var responses = {
    success: function (data, code) {
        if (data === void 0) { data = {}; }
        if (code === void 0) { code = 200; }
        return {
            statusCode: code,
            headers: responseHeaders,
            body: JSON.stringify(data)
        };
    },
    error: function (error) {
        return {
            statusCode: error.code || 500,
            headers: responseHeaders,
            body: JSON.stringify(error)
        };
    }
};
exports.getWeather = function (event, context) { return __awaiter(void 0, void 0, void 0, function () {
    var results, city, response, queryResult, queryResults, err_1, err_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                city = event.pathParameters.city_name;
                console.log(city);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 8, , 9]);
                return [4 /*yield*/, axios({
                        url: weatherApiUrl + "&q=" + city + "&appid=" + apiKey,
                        method: "get",
                        port: 443,
                        responseType: JSON
                    })];
            case 2:
                response = _a.sent();
                results = {
                    description: response.data.weather[0].description,
                    temp: response.data.main.temp,
                    feels_like: response.data.main.feels_like,
                    name: response.data.name,
                    error: null
                };
                return [4 /*yield*/, mysql.query("SELECT city_name as name FROM city where city_name = ?", [response.data.name])];
            case 3:
                queryResult = _a.sent();
                console.log('queryResult@' + queryResult + '@');
                if (!(queryResult.length < 1)) return [3 /*break*/, 7];
                _a.label = 4;
            case 4:
                _a.trys.push([4, 6, , 7]);
                console.log('inside !queryResult' + queryResult);
                return [4 /*yield*/, mysql
                        .transaction()
                        .query("INSERT INTO city (city_name,is_preferred) VALUES(?,?)", [
                        response.data.name,
                        0,
                    ])
                        .rollback(function (e) {
                        /* do something with the error */
                        console.log(e);
                        results = "connection issue";
                    }) // optional
                        .commit()];
            case 5:
                queryResults = _a.sent();
                return [3 /*break*/, 7];
            case 6:
                err_1 = _a.sent();
                if (err_1.code === "ER_DUP_ENTRY") {
                    console.log(err_1);
                    results = "City already exists";
                }
                else {
                    console.log(err_1);
                    results = "Error in creating city in getWeather()";
                }
                return [3 /*break*/, 7];
            case 7: return [3 /*break*/, 9];
            case 8:
                err_2 = _a.sent();
                console.error("getWeather error: ", err_2);
                results = {
                    error: "Sorry, there was a problem with the weather service."
                };
                return [3 /*break*/, 9];
            case 9: return [2 /*return*/, responses.success(results)];
        }
    });
}); };
exports.getAutoCompleteCityName = function (event, context) { return __awaiter(void 0, void 0, void 0, function () {
    var city_name, results;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                city_name = event.queryStringParameters.city;
                return [4 /*yield*/, mysql.query("SELECT city_name as name FROM city where city_name like ?", ["%" + city_name + "%"])];
            case 1:
                results = _a.sent();
                // Return the results
                return [2 /*return*/, responses.success(results)];
        }
    });
}); };
exports.getPreferredCities = function (event, context) { return __awaiter(void 0, void 0, void 0, function () {
    var results;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, mysql.query("SELECT city_name FROM city where is_preferred is true")];
            case 1:
                results = _a.sent();
                // Return the results
                console.log(JSON.stringify(results));
                return [2 /*return*/, results];
        }
    });
}); };
exports.getAllCities = function (event, context) { return __awaiter(void 0, void 0, void 0, function () {
    var results;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, mysql.query("SELECT * FROM city")];
            case 1:
                results = _a.sent();
                // Return the results
                return [2 /*return*/, results];
        }
    });
}); };
exports.updatePreferredForCity = function (event, context) { return __awaiter(void 0, void 0, void 0, function () {
    var results, cityName, subscribeAlert;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                cityName = event.queryStringParameters.city;
                subscribeAlert = event.queryStringParameters.isPreferred;
                console.log(subscribeAlert);
                console.log(cityName);
                return [4 /*yield*/, mysql
                        .transaction()
                        .query("Update city set is_preferred = ? where city_name =?", [
                        subscribeAlert,
                        cityName,
                    ])
                        .rollback(function (e) {
                        /* do something with the error */
                        console.log(e);
                        results = "connection issue";
                    }) // optional
                        .commit()];
            case 1:
                // Run query
                results = _a.sent();
                // Return the results
                return [2 /*return*/, responses.success(results)];
        }
    });
}); };
exports.createCity = function (event, context) { return __awaiter(void 0, void 0, void 0, function () {
    var results, data, err_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                data = JSON.parse(event.body);
                console.log(data.city);
                console.log(data.isPreferred);
                return [4 /*yield*/, mysql
                        .transaction()
                        .query("INSERT INTO city (city_name,is_preferred) VALUES(?,?)", [
                        data.city,
                        data.isPreferred,
                    ])
                        .rollback(function (e) {
                        /* do something with the error */
                        console.log(e);
                        results = "connection issue";
                    }) // optional
                        .commit()];
            case 1:
                // Run query
                results = _a.sent();
                return [3 /*break*/, 3];
            case 2:
                err_3 = _a.sent();
                if (err_3.code === "ER_DUP_ENTRY") {
                    console.log(err_3);
                    results = "City already exists";
                }
                else {
                    console.log(err_3);
                    results = "Error in createCity";
                }
                return [3 /*break*/, 3];
            case 3: 
            // Return the results
            return [2 /*return*/, results];
        }
    });
}); };
exports.sendWeatherAlert = function (event) { return __awaiter(void 0, void 0, void 0, function () {
    var funcName, params, resp, params1, params2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                funcName = "sendWeatherAlert";
                console.log("entering" + funcName);
                params = {
                    FunctionName: "serverless-weather-app-dev-getPreferredCities",
                    InvocationType: "RequestResponse",
                    Payload: '{ "foo" : "bar" }'
                };
                return [4 /*yield*/, lambda
                        .invoke(params, function (err, data) {
                        if (err) {
                            throw err;
                        }
                        else {
                            console.log("FIRST serverless-weather-app-dev-getPreferredCities invoked: " +
                                data.Payload);
                            //resp = data.Payload;
                            resp = JSON.stringify(data.Payload);
                        }
                    })
                        .promise()];
            case 1:
                _a.sent();
                params1 = {
                    FunctionName: "serverless-weather-app-dev-getWeatherForEmail",
                    InvocationType: "RequestResponse",
                    Payload: resp
                };
                return [4 /*yield*/, lambda
                        .invoke(params1, function (err, data) {
                        if (err) {
                            throw err;
                        }
                        else {
                            var response = JSON.parse(data.Payload);
                            console.log("SECOND serverless-weather-app-dev-getWeatherForEmail invoked: " +
                                response.body);
                            resp = JSON.stringify(response.body);
                        }
                    })
                        .promise()];
            case 2:
                _a.sent();
                params2 = {
                    FunctionName: "serverless-weather-app-dev-sendEmail",
                    InvocationType: "RequestResponse",
                    Payload: resp
                };
                return [4 /*yield*/, lambda
                        .invoke(params2, function (err, data) {
                        if (err) {
                            throw err;
                        }
                        else {
                            var response = JSON.parse(data.Payload);
                            console.log("SECOND serverless-weather-app-dev-getWeatherForEmail invoked: " +
                                response.body);
                            resp = JSON.stringify(response.body);
                        }
                    })
                        .promise()];
            case 3:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); };
exports.getWeatherForEmail = function (event, context) { return __awaiter(void 0, void 0, void 0, function () {
    var weatherJsonObj, weatherJsonArr, cityArr, i, city, response, err_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                weatherJsonArr = [];
                console.log(JSON.parse(event));
                cityArr = JSON.parse(event);
                i = 0;
                _a.label = 1;
            case 1:
                if (!(i < cityArr.length)) return [3 /*break*/, 7];
                city = cityArr[i].city_name;
                console.log(city);
                _a.label = 2;
            case 2:
                _a.trys.push([2, 4, , 5]);
                return [4 /*yield*/, axios({
                        url: weatherApiUrl + "&q=" + city + "&appid=" + apiKey,
                        method: "get",
                        port: 443,
                        responseType: JSON
                    })];
            case 3:
                response = _a.sent();
                weatherJsonObj = {
                    description: response.data.weather[0].description,
                    temp: response.data.main.temp,
                    feels_like: response.data.main.feels_like,
                    name: response.data.name,
                    error: null
                };
                return [3 /*break*/, 5];
            case 4:
                err_4 = _a.sent();
                console.error("getWeather error: ", err_4);
                weatherJsonObj = {
                    error: "Sorry, there was a problem with the weather service."
                };
                return [3 /*break*/, 5];
            case 5:
                console.log("got weather sir" + JSON.stringify(weatherJsonObj));
                weatherJsonArr.push(weatherJsonObj);
                _a.label = 6;
            case 6:
                i++;
                return [3 /*break*/, 1];
            case 7:
                console.log("exiting getWeatherForEmail" + JSON.stringify(weatherJsonArr));
                return [2 /*return*/, {
                        statusCode: 200,
                        body: JSON.stringify(weatherJsonArr)
                    }];
        }
    });
}); };
