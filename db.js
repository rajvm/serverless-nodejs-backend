const Sequelize = require('sequelize')
const communityEntities = require('./models/CommunityEntities')

const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
        dialect: 'mysql',
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        // disable logging; default: console.log
        logging: false
    }
    )

const GeoAddressEntity = communityEntities.city(sequelize, Sequelize)
const Models = {GeoAddressEntity}
const connection = {}

module.exports.sequelize = async () => {
    if (connection.isConnected) {
        console.log('=> Using existing connection.')
        return sequelize
    }
    //await sequelize.sync()
    await sequelize.authenticate()
    connection.isConnected = true
    console.log('=> Created a new connection.')  
    return sequelize
    }
    module.exports.models = async () => {
    if (!connection.isConnected) {
        await sequelize.authenticate()
        connection.isConnected = true
        console.log('=> Created a new connection.')  
    }
    return Models
}
