const AWS = require('aws-sdk');
const SES = new AWS.SES();


/**
 * Base response HTTP headers
 */
 const responseHeaders = {
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin' : '*',        // Required for CORS support to work
    'Access-Control-Allow-Credentials' : true   // Required for cookies, authorization headers with HTTPS 
  }
  
  /**
  * HTTP response templates
  */
  const responses = {
    success: (data={}, code=200) => {
        return {
            'statusCode': code,
            'headers': responseHeaders,
            'body': JSON.stringify(data)
        }
    },
    error: (error) => {
        return {
            'statusCode': error.code || 500,
            'headers': responseHeaders,
            'body': JSON.stringify(error)
        }
    }
  }


exports.handler = async (event) => {
    console.log("event" + event);
    //check this raja
    // const {to,from,subject,text} = JSON.parse(event.body)
    //console.log("event.body@@" + event.body);
    //console.log("JSON.parse(event.body)@@" + JSON.parse(event.body));
    console.log("JSON.stringify(event)@@"+JSON.stringify(event));
    console.log("JSON.parse(event)@@"+JSON.parse(event));
    let weatherArr = JSON.parse(event);
    const to = 'rajvm@presidio.com';
    const from = 'rajvm@presidio.com';
    const subject = 'Weather Alert';
    let text = '';

    for (let i = 0; i < weatherArr.length; i++) {
        const cityWeather = weatherArr[i];
        let result;
        
        result = 'Weather in '+ cityWeather.name + ' feels like '+ cityWeather.feels_like + ' with actual temperature of ' + cityWeather.temp + '.' + '\n' + 'We can expect '+ cityWeather.description+'\n';
        if(text.length<1){
            text = result;
        }else{
            text = text + result;
        }
    }

    console.log('FINAL RESULT'+text);

    if(!to || !from || !subject || !text){
        return responses.error({ message: 'to, from, subject and text are all required in the body'});
    
    }

    const params = {
        Destination: {
            ToAddresses: [to],
        },
        Message: {
            Body: {
                Text: { Data: text },
            },
            Subject: { Data: subject },
        },
        Source: from,
    };

    try {
        await SES.sendEmail(params).promise();
        return responses.success({message: 'Success'});
    } catch (error) {
        console.log('error sending email ', error);
        return responses.error({ message: 'The email failed to send' });
    }


};
